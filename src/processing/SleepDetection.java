package processing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class SleepDetection {
	final int MAXSIZE = 20 * 24 * 60;	
	double[] inputData;
	String [] times;
	int dataSize = 0;
	
	public void readData(String path) throws Exception {
		inputData = new double[MAXSIZE];
		times = new String[MAXSIZE];
		BufferedReader in = null;
		in = new BufferedReader(new FileReader(path));
		String line;
		int t = 0;	
		line = in.readLine(); // for the header line
		while ((line = in.readLine()) != null) {
			String[] tokens = line.trim().split(",");
			times[t] = tokens[0];
			inputData[t] = Double.parseDouble(tokens[1]);
			t++;
		}
		dataSize = t;
		in.close();						
	}
	
	//---------------------------Create Sadeh Data Base for just actigraph data-----------------------
	public void createSadehDB(String path, String outPath, String outName) {				
		PrintWriter out = null;
		try {			
			String dbName = outPath + outName;
			String dbLabel = "TIME_STAMP,ACC,MU,SIGMA,LOGACT,NAT";
			File dbDir = new File(outPath);
	    	if (!dbDir.isDirectory()) {
	    		dbDir.mkdirs();
			}    	
	        File dbFile = new File(dbName);
	        if (dbFile.exists()) {	
	        	dbFile.delete(); 
		    }
	        dbFile.createNewFile();               
        	out = new PrintWriter(new FileWriter(dbName));
        	out.append(dbLabel + "\n");
        	
            for (int m = 10; m < dataSize - 5; m++) {
            	double[] sadeh = calculateSadeh(m);
            	String msg =  times[m] + "," + sadeh[0] + "," + sadeh[1] + "," + sadeh[2]  + "," +
            			sadeh[3] + "," + sadeh[4];
				out.append(msg + "\n");
			}
            out.flush();
		} catch (Exception e) {	
			e.printStackTrace();
		} finally {									    
	        if (out != null) {
	        	out.flush();
	            out.close();
	        }	
		}
	}	
	
	public void sadehClassify(String path, String dbName, int algorithm, String outName) {	
		
		String fileName = path + outName;
		String fileLabel = "TIME_STAMP,LABEL";		 
	    PrintWriter out = null;
		BufferedReader in = null;
		
		try {
	    	File outputDir = new File(path);
			if (!outputDir.isDirectory()) {
				outputDir.mkdirs();
			} 
		    File f = new File(fileName);
		    if (f.exists()) {	
		    	f.delete(); 
		    }
		    f.createNewFile();
	    	out = new PrintWriter(new FileWriter(fileName,true));
	    	out.append(fileLabel + "\n");
			
			in = new BufferedReader(new FileReader(path + dbName));
			String line;
			line = in.readLine();
			while ((line = in.readLine()) != null) {
				String[] tokens = line.trim().split(",");
				double[] values = {Double.parseDouble(tokens[1]), Double.parseDouble(tokens[2]), Double.parseDouble(tokens[3]), Double.parseDouble(tokens[4]), Double.parseDouble(tokens[5])};
				String msg =  tokens[0] + "," + sadeh(values);
				out.append(msg + "\n");				
			}			
			in.close();	
		} catch (IOException e) {
			e.printStackTrace();
	    } finally {
	    	out.flush();
	        if (out != null)
	        	out.close();
	        if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
	    }
	    
	}
	
	public void SazonovClassify(String path, String outName) {	
		
		String fileName = path + outName;
		String fileLabel = "TIME_STAMP,SAZONOV_LABEL";		 
	    PrintWriter out = null;
		
		try {
	    	File outputDir = new File(path);
			if (!outputDir.isDirectory()) {
				outputDir.mkdirs();
			} 
		    File f = new File(fileName);
		    if (f.exists()) {	
		    	f.delete(); 
		    }
		    f.createNewFile();
	    	out = new PrintWriter(new FileWriter(fileName,true));
	    	out.append(fileLabel + "\n");			
			
			for (int i = 8; i < dataSize; i++) {
				String msg =  times[i] + "," + sazonov(i);
				out.append(msg + "\n");				
			}	
		} catch (IOException e) {
			e.printStackTrace();
	    } finally {
	    	out.flush();
	        if (out != null)
	        	out.close();
	    }
	    
	}
	
	
	public void KripkeClassify(String path, String outName) {	
		
		String fileName = path + outName;
		String fileLabel = "TIME_STAMP,KRIPKE_LABEL";		 
	    PrintWriter out = null;
		
		try {
	    	File outputDir = new File(path);
			if (!outputDir.isDirectory()) {
				outputDir.mkdirs();
			} 
		    File f = new File(fileName);
		    if (f.exists()) {	
		    	f.delete(); 
		    }
		    f.createNewFile();
	    	out = new PrintWriter(new FileWriter(fileName,true));
	    	out.append(fileLabel + "\n");			
			
			for (int i = 10; i < dataSize - 2; i++) {
				String msg =  times[i] + "," + kripke(i);
				out.append(msg + "\n");				
			}	
		} catch (IOException e) {
			e.printStackTrace();
	    } finally {
	    	out.flush();
	        if (out != null)
	        	out.close();
	    }
	    
	}

	//-----------------------------------------
	public double[] calculateSadeh(int index) {
		double mu = 0;		//Mean activity on a 11-min window centered on current epoch
		double sigma = 0;	//standard deviation of activity for the last 6min
		double logAct = 0;	//natural log of the current epoch activity + 1
		double nat = 0;		//the number of epochs with (50 <= epoch activity < 100) on a 11-min window centered on current epoch. for Wocket, I consider epoch activity > 100.		
		
		int cnt = 0;
		mu = 0;
		nat = 0;
		for (int j = -5; j <= 5; j++) {
			if ((index + j >= 0) && (index + j < dataSize)) {					
				double acc = inputData[index + j];
				mu += acc / 5;
				if (acc > 480)					
					nat++;	
				cnt++;
			}
		}
		mu /= cnt;
		//-------------------------------------
		double mean = 0;
		cnt = 0;
		for (int j = -5; j <= 0; j++) {
			if (index + j >= 0) {
				mean += inputData[index + j] / 5;					
				cnt++;
			}
		}
		if (cnt != 0) { 					
			mean /= cnt;
			//-------------------------------------
			for (int j = -5; j <= 0; j++) {
				if (index + j >= 0) {											
					sigma += Math.pow((inputData[index + j] / 5 - mean), 2);
					//sigma += Math.pow((epoch30List.get(i + j).getAcc() - mean), 2);
				}
			}
			sigma = Math.sqrt(sigma);
		} else 
			sigma = 0;
		//--------------------------------------			
		logAct = Math.log(inputData[index] / 5) + 1;
		
		double[] a = {inputData[index], mu, sigma, logAct, nat};
		return a;
	}
//------------------------------------------------------------------------------------
	
	
	
	public static int sadeh(double[] input) {		
		//constant, MU, SIGMA, LOGACT,NAT
		//original
		//double[] COEF = { 7.601, -0.065, -0.056, -0.0703, -1.08};
		//Wocket wrirt weka
		double[] COEF = {4.5259, -0.0079, -0.0025, -0.6936, -0.2398};
		//Wocket Ankle Weka
		//double[] COEF = {3.0396, -0.0049, -0.0029, -0.5444, -0.3629};
		//Actigraph wrist
		//double[] COEF = {6.4524, -0.0037, -0.003, -1.19, -0.3001};
		//Actigraph ankle
		//double[] COEF = {6.5209, -0.023, -0.0049, -1.3208, 0.0865};

		
		double si = COEF[0];
		for (int j = 1; j < 5; j++) {
			si += COEF[j] * input[j];
		}
		double	psi = 1 / (1 + Math.exp(-si));
		return (psi < 0.5) ? 1 : 0;
	}
	
	
	
	public int sazonov(int index) { //-8..0
		//original					cons	-8		-7		-6			-5			-4			-3		-2		-1			0
		//double[] COEFFICIENT = {1.99604, -0.10207, -0.073, -0.07494, -0.08108, -0.08917, -0.10194, -0.09975, -0.09746, -0.1945};
		//Weka Wocket Wrist
		double[] COEFFICIENT = {2.6493, -0.0009, -0.0001, -0.0005, -0.0003, -0.0006, -0.0005, -0.0011, -0.0013, -0.002};
		
		double si = 0;
		double psi = 0;		
		
		si = COEFFICIENT[0];
		for (int j = -8; j < 1; j++) 
			si += COEFFICIENT[j + 9] * inputData[index + j];
		psi = 1 / (1 + Math.exp(-si));
		return (psi < 0.5) ? 1 : 0;
	}	
	
	//--------------------------------------------------------------------------
	public int kripke(int index) { //-10..2					
		//Wocket Wrist
		double[] COEFFICIENT = {2.8873, -0.0007, -0.0001, -0.0005, -0.0001, -0.0004, -0.0003,
				-0.0005, -0.0005, -0.0009, -0.0013, -0.0015, -0.0002, -0.0006};
		
		
		double si = COEFFICIENT[0];
		for (int j = -10; j < 3; j++) 
			si += COEFFICIENT[j + 11] * inputData[index + j];
		double	psi = 1 / (1 + Math.exp(-si));
		return (psi < 0.5) ? 1 : 0;
	}	
	
	//------------Find changes from sleep to wake and vice versa---------
	public void findStateChanges(String path, String id, String fName, String outName) {
		try {	
			
			String[] swLabels = new String[MAXSIZE];
			String[] swTimes = new String[MAXSIZE];
			BufferedReader in = null;
			in = new BufferedReader(new FileReader(path + fName));
			String line = in.readLine(); // for the header line
			int t = 0;
			while ((line = in.readLine()) != null) {
				String[] tokens = line.trim().split(",");
				swTimes[t] = tokens[0];
				swLabels[t] = tokens[1];
				t++;
			}
			int size = t;
			in.close();	
			
			PrintWriter out = null;
	    	File outputDir = new File(path);
			if (!outputDir.isDirectory()) {
				outputDir.mkdirs();
			} 
		    File f = new File(path + outName);
		    if (!f.exists()) {
		    	f.createNewFile();
		    }
	    	out = new PrintWriter(new FileWriter(path + outName, true));
	    	out.append(id.substring(0, 9) + ",");
	    	
			String majorstate = "0";
			String currentState = swLabels[0];
			int cnt = 0;
			int tempCnt = 0;
			
			for (int i = 1; i < size; i++) {
				 if (swLabels[i] == null)
					 break;
				 if (swLabels[i].equals(currentState)) {
					 cnt += 1 + tempCnt;
					 tempCnt = 0;
					 if (cnt >= 30 && !majorstate.equals(currentState)) {
						 out.append(swTimes[i - 30] + ",");
						 majorstate = currentState;
					 }
						 
				 } else {
					 tempCnt++;
					 if (tempCnt > 1) {
						 cnt = tempCnt;
						 tempCnt = 0;
						 currentState = swLabels[i];
					 }
				 }
			}
			out.append("\n");
			out.flush();
			if (out != null)
	        	out.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	        
		}		
	}
		
	
}
