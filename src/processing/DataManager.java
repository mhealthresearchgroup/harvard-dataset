package processing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DataManager {
	
		
	public static void main(String[] args) {
		
		//String inputPath = "G:/Harvard I-Min data/";
		//String localPath = "c:/harvardData/";
		//String outPath = localPath;
		
		/*String rawPath = "C:/Users/Aida/Data/Harvard I-Min data/3.RAW/";
		String timedPath = "C:/Users/Aida/Data/Harvard I-Min data/4.Timed/";
		String sumPath = "C:/Users/Aida/Data/Harvard I-Min data/5.OneMinSum/";
		String correctPath = "C:/Users/Aida/Data/Harvard I-Min data/6.CorrectTime/";
		String trimmedPath = "C:/Users/Aida/Data/Harvard I-Min data/7.Trimmed/";

		String oneSecPath = "C:/Users/Aida/Data/Harvard I-Min data/8.OneSecSum/";
		String oneSecCorPath = "C:/Users/Aida/Data/Harvard I-Min data/9.OneSecCorrect/";
		String oneSecTrimPath = "C:/Users/Aida/Data/Harvard I-Min data/10.OneSecTrimmed/";*/
		
		//String rawPath = "C:/Users/Aida/Data/Harvard I-Min data/Test/";
		String rawPath = "C:/Users/Aida/Data/Aida Actigraph Test/";
		String sumPath  = rawPath;
		String correctPath = rawPath;
		String trimmedPath  = rawPath;
		String oneSecPath = rawPath;
		String timedPath = rawPath;
		
		String timed = "timed.csv";
		String oneSec = "oneSec.csv";
		String summary = "summary.csv";
		String tcorrect = "tcorrect.csv";
		String trimmed = "trimmed.csv";
		
		File pathFile = new File(rawPath);
		String[] files = pathFile.list(new FilenameFilter() {							
			@Override
			public boolean accept(File dir, String filename) {
				return filename.contains("RAW");
				//return filename.contains("tcorrect");
			}
		});
		
		for (int k = 0; k < files.length; k++) {
			String id = files[k].substring(0, 10) + "-";
			
			/*outPath += id.substring(0, 9) + "/";
			File dir = new File(outPath);
			if (!dir.isDirectory()) {
	    		dir.mkdirs();
			}*/
			//put time stamp for the raw file
			Converter.agRaw2agTimed(rawPath, files[k], timedPath, id + timed);	//raw to timed csv
			
			//----------------1 min summary----------------------
			//Converter.agTimed2agCount(timedPath, id + timed, sumPath, id + summary, 30);	
			//correcTime(id.substring(0, 9), sumPath, correctPath, id + summary, id + tcorrect);			
			//trimFile(correctPath, trimmedPath, id + tcorrect, id + trimmed);
			
			//----------------1 sec summary----------------------
			Converter.oneSecCount(timedPath, id + timed, oneSecPath, id + oneSec, 30);
			//correcTime(id.substring(0, 9), oneSecPath, oneSecCorPath, id  + oneSec, id + tcorrect);			
			//trimFile(oneSecCorPath, oneSecTrimPath, id + tcorrect, id + trimmed);
			
			
			System.out.println("Done with: " + id);
			
			/*File file = new File(outPath, id + timed);
	        if (file.exists()) {	
	        	file.delete(); 
		    }*/
			
	        //Sleep Processing
			/*
			String sadehDB = "sadehDB.csv";
			String sadehStates = "sadehStates.csv";
			String KripkeStates = "kripkeStates.csv";
			String sazovovStates = "sazonovStates.csv";
			String SadehLabels = "Sadehlabels.csv";
			String kripkeLabels = "kripkeLabels.csv";
			String sazonovLabels = "sazonovLabels.csv";
			SleepDetection sd = new SleepDetection();
			
			try {
				sd.readData(outPath + id + trimmed);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			sd.createSadehDB(outPath, outPath, id + sadehDB);		
			sd.sadehClassify(outPath, id + sadehDB, 1, id + sadehStates);
			sd.findStateChanges(localPath, id, id + sadehStates, SadehLabels);
			
			sd.SazonovClassify(outPath, id + sazovovStates);
			sd.findStateChanges(localPath, id, id + sazovovStates, sazonovLabels);
			
			sd.KripkeClassify(outPath, id + KripkeStates);
			sd.findStateChanges(localPath, id, id + KripkeStates, kripkeLabels);*/
		}
		
		//Create a file matching the IDs, zipcodes & time zones
		//ZipTimeManager.makeTimeZip();
	}
	
			
	//-----------------------------
	public static void trimFile(String inPath, String outPath, String fName, String outName) {
		String fileName = outPath + outName;
		String fileLabel = "TIME_STAMP,VALUE";		 
	    PrintWriter out = null;
	    //final int windSize = 7 * 24 * 60; //one min
	    final int windSize = 7 * 24 * 60 * 60; // oneSec
	    String line = "";
	    int t = 0;
		try {
	    	File outputDir = new File(outPath);
			if (!outputDir.isDirectory()) {
				outputDir.mkdirs();
			} 
		    File f = new File(fileName);
		    if (f.exists()) {	
		    	f.delete(); 
		    }
		    f.createNewFile();
	    	out = new PrintWriter(new FileWriter(fileName,true));
	    	out.append(fileLabel + "\n");			
						
			int maxSize = windSize * 5; //whole datasize
	    	String[] times = new String[maxSize] ;
			double[] values = new double[maxSize] ;
			BufferedReader in = new BufferedReader(new FileReader(inPath + fName));			
			
			//int t = 0;			
			int end = 0;
			double maxSum = 0;
			double sum = 0;
			
			line = in.readLine(); // skip the label
			
			while ((line = in.readLine()) != null) {
				String[] tokens = line.trim().split(",");
				times[t] = tokens[0];
				values[t] = Double.parseDouble(tokens[1]);
				
				//Find the window of 7 days with maximum total activity as the study period
				if (t == windSize - 1) {
					for (int i = 0; i <= t; i++) {
						sum += values[i];
					}					
				} else if (t >= windSize) {
					sum = sum + values[t] - values[t - windSize];
				}
				
				if (t > 1000) { // Eliminate the first day,as the sensor was on mail for sure 
					if (sum > maxSum) {
						maxSum = sum;
						end = t;
					}
				}
				t++;				
			}
			for (int i = end - windSize + 1; i <= end; i++) {
				String msg =  times[i] + "," + values[i];
				out.append(msg + "\n");	
			}
			in.close();	
			
		} catch (IOException e) {
			e.printStackTrace();
	    } catch (Exception e) {
	    	System.out.println("t: " + t);
			e.printStackTrace();
	    } finally {
	    	out.flush();
	        if (out != null)
	        	out.close();
	    }
	}
	
	//-----------------------------
	
	//if daylight saving is applied check the date of the study to see if it applied
	//correct the time in the file
	public static void correcTime(String ID, String inPath, String outPath, String inFile, String outFile) {
		SimpleDateFormat mHealthFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		PrintWriter out = null;
		BufferedReader in = null;
		String line;
		String tzLine;
		int diff = 0;
		try {
			String myPath = "C:/Users/Aida/Data/Harvard I-Min data/Harvard DataSet info/timeZip.csv";	
			BufferedReader tzReader = new BufferedReader(new FileReader(myPath));
			while ((tzLine = tzReader.readLine()) != null) {
				if (tzLine.contains(ID)) {					
					break;
				}
			}
			int tz = Integer.parseInt(tzLine.trim().split(",")[2]);
						
			in = new BufferedReader(new FileReader(inPath + inFile));	
			
			if (tzLine != null) {				
				diff = -tz - 5; // the origin is Boston area ; GMT - 5
				
				out = new PrintWriter(new FileWriter(outPath + outFile, true));
				out.append("TIME_STAMP,VALUE\n");
				
				if (tzLine.trim().split(",")[3].equals("1")) { //ds=1
					if(diff == 0) {
						if (tzReader != null) {
							tzReader.close();
						}
						line = in.readLine();//ignore label
						while ((line = in.readLine()) != null) {
							out.append( line + "\n");
						}
						System.out.println("No correction time is needed for " + inFile);
					} else {
						//apply the correction for all the file						
						line = in.readLine();//ignore label
						while ((line = in.readLine()) != null) {
							Date date = mHealthFormat.parse(line.split(",")[0]);
							date.setTime(date.getTime() + (diff * 3600000));
							out.append( mHealthFormat.format(date) + "," + line.split(",")[1] + "\n");
						}
						System.out.println("Times are corrected for " + inFile);
					}
				} else {//ds=0 //the only one ID: 300754239					
					Date start = mHealthFormat.parse("2013-03-10 02:00:00.000"); // March 10th 2013
					Date end = mHealthFormat.parse("2013-11-03 02:00:00.000"); // November 3rd 2013
										
					line = in.readLine();//ignore label
					while ((line = in.readLine()) != null) {
						diff = -tz - 5;
						Date date = mHealthFormat.parse(line.split(",")[0]);
						if (date.before(end) && date.after(start)) {
							diff += 1; 
						}
						date.setTime(date.getTime() + (diff * 3600000));
						out.append( mHealthFormat.format(date) + "," + line.split(",")[1] + "\n");	
						//line = in.readLine();
					}
					System.out.println("Times are corrected for " + inFile);
				}
			} 
			
			if (tzReader != null) {
				tzReader.close();
			}
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	//------------------
	public static void analysis() {
		
	}
	//------------------
	
	
}
